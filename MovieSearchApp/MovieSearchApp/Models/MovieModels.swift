//
//  File.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import Foundation

public enum APIError: Error {
 case invalidJSON
 case invalidData
 case invalidURL
}

public struct Movie: Codable {
  let voteAverage: Double
  let overView: String
  let posterPath: String?
  let releaseDate: String?
  let title: String
  var posterURL: String = ""
  
  enum CodingKeys: String, CodingKey {
    case voteAverage = "vote_average"
    case overView = "overview"
    case posterPath = "poster_path"
    case releaseDate = "release_date"
    case title
  }
  
  public init(voteAverage: Double,
              overView: String,
              posterPath: String,
              releaseDate: String,
              title: String,
              posterURL: String) {
    self.voteAverage = voteAverage
    self.overView = overView
    self.posterPath = posterPath
    self.releaseDate = releaseDate
    self.title = title
    self.posterURL = posterURL
  }
}

public struct MovieResult: Codable {
  let movies: [Movie]
  let page: Int
  let totalPages: Int
  
  enum CodingKeys: String, CodingKey {
    case totalPages = "total_pages"
    case movies = "results"
    case page
  }
  
  public init(movies: [Movie],
              page: Int,
              totalPages: Int) {
    self.movies = movies
    self.page = page
    self.totalPages = totalPages
  }
}

public struct MovieModel: Codable {
  let voteAverage: Double
  let overView: String
  let releaseDate: String
  let title: String
  let posterURL: String
  
  public init(voteAverage: Double,
              overView: String,
              releaseDate: String,
              title: String,
              posterURL: String) {
    self.voteAverage = voteAverage
    self.overView = overView
    self.releaseDate = releaseDate
    self.title = title
    self.posterURL = posterURL
  }
}
