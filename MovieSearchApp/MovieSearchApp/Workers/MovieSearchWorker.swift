//
//  MovieSearchWorker.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

public protocol MovieSearchStoreProtocol {
  func getMovieList(page: Int, textTitle: String, completion: @escaping(Result<MovieResult, APIError>) -> Void)
}

public class MovieSearchWorker {

  var store: MovieSearchStoreProtocol
  var storage: DeviceStorageProtocol

  init(store: MovieSearchStoreProtocol, storage: DeviceStorageProtocol) {
    self.store = store
    self.storage = storage
  }

  // MARK: - Business Logic

  public func getMoviesFromApi(_ completion: @escaping (Result<MovieResult, APIError>) -> Void, page: Int, textTitle: String) {
    
    store.getMovieList(page: page, textTitle: textTitle, completion: { (result) in
      completion(result)
    })
  }
  
  func saveSearchTexts(searchTexts: [String]) {
    storage.saveSearchTextList(textList: searchTexts)
  }
  
  func getHistory() -> [String] {
    return storage.getSearchText()
  }
  
  func getFavoriteMovies() -> [MovieModel] {
    return storage.getFavouriteMovie()
  }
  
  func saveFavouriteMovies(favouriteMovies: [MovieModel]) {
    storage.saveFavouriteList(favouriteList: favouriteMovies)
  }
}
