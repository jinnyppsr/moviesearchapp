//
//  MovieSearchStore.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import Foundation
import Alamofire

public class MovieSearchStore: MovieSearchStoreProtocol {  
  let headers: HTTPHeaders = [
    "api-key": "66c42ab959071d2a12a418a73f6411d2b6634d60"
  ]
  public func getMovieList(page: Int, textTitle: String, completion: @escaping(Result<MovieResult, APIError>) -> Void) {
    let baseURL: String = "http://scb-movies-api.herokuapp.com/api/movies/search?query=\(textTitle)&page=\(page)"
    AF.request(baseURL, headers: headers).responseJSON{ (data) in
      switch data.result {
      case .success:
        do {
         let decoder = JSONDecoder()
         let result = try decoder.decode(MovieResult.self, from: data.data!)
         completion(.success(result))
        } catch let error {
         print(error)
         completion(.failure(.invalidJSON))
        }
      case .failure(let error):
        print(error)
        completion(.failure(APIError.invalidData))
      }
    }
  }
}
