//
//  DeviceStorage.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 1/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import Foundation

protocol DeviceStorageProtocol {
  func insertSearchText(text: String)
  func saveSearchTextList(textList: [String])
  func getSearchText() -> [String]
  func saveFavouriteList(favouriteList: [MovieModel])
  func getFavouriteMovie() -> [MovieModel]
}

public class DeviceStorage: DeviceStorageProtocol {
  func insertSearchText(text: String) {
    var searchTexts = self.getSearchText()
    searchTexts.append(text)
    UserDefaults.standard.set(try? PropertyListEncoder().encode(searchTexts), forKey: "SearchText")
  }
  
  func saveSearchTextList(textList: [String]) {
    UserDefaults.standard.set(try? PropertyListEncoder().encode(textList), forKey: "SearchText")
  }
  
  func getSearchText() -> [String] {
    if let searchTexts = UserDefaults.standard.value(forKey: "SearchText") as? Data {
      let searchTextsDecode = try? PropertyListDecoder().decode([String].self, from: searchTexts)
      if let text = searchTextsDecode {
        return text
      } else {
        return []
      }
    }
    return [String]()
  }
  
  func saveFavouriteList(favouriteList: [MovieModel]) {
    UserDefaults.standard.set(try? PropertyListEncoder().encode(favouriteList), forKey: "FavouriteMovie")
  }
  
  func getFavouriteMovie() -> [MovieModel] {
    if let favouriteMovies = UserDefaults.standard.value(forKey: "FavouriteMovie") as? Data {
      let favouriteDecode = try? PropertyListDecoder().decode([MovieModel].self, from: favouriteMovies)
      if let favourite = favouriteDecode {
        return favourite
      } else {
        return []
      }
    }
    return [MovieModel]()
  }
  
}
