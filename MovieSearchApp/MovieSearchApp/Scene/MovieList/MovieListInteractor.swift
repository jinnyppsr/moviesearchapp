//
//  MovieListInteractor.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 1/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieListInteractorInterface {
  func sortMovieList(request: MovieList.GetSortList.Request)
  func loadMoreMovie(request: MovieList.LoadMoreMovie.Request)
  var movieResult: MovieResult? { get set }
  var searchText: String { get set }
  var pageMore: Int { get set }
}

class MovieListInteractor: MovieListInteractorInterface {
  var presenter: MovieListPresenterInterface!
  var worker: MovieSearchWorker?
  
  var movieResult: MovieResult?
  var searchText: String = ""
  var sortedList: [Movie] = []
  var pageMore: Int = 1

  // MARK: - Business logic
  
  func sortMovieList(request: MovieList.GetSortList.Request) {
    if let list = movieResult?.movies {
      var sortList = list
      sortList.sort(by: {
        if let date1 = $0.releaseDate, let date2 = $1.releaseDate {
          return date1 > date2
        }
        return false
      })
      sortedList = sortList
    }
    if let page = self.movieResult?.page {
      let response = MovieList.GetSortList.Response(page: page, movies: sortedList)
      self.presenter.presentMovieList(response: response)
    }
  }
  
  func loadMoreMovie(request: MovieList.LoadMoreMovie.Request) {
    pageMore += 1
    worker?.getMoviesFromApi({ [weak self] (result) in
      guard let strongSelf = self else {
        return
      }
      switch result {
      case .success(let movieResult):
        var newList = movieResult.movies
        newList.sort(by: {
          if let date1 = $0.releaseDate, let date2 = $1.releaseDate {
            return date1 > date2
          }
          return false
        })
        strongSelf.sortedList = newList
        let response = MovieList.LoadMoreMovie.Response(result: .success(newList))
        self?.presenter.presentLoadMore(response: response)
      case .failure(let error):
        print("xxx")
        let response = MovieList.LoadMoreMovie.Response(result: .failure(error))
        self?.presenter.presentLoadMore(response: response)
      }
      
      }, page: pageMore, textTitle: request.searchText)
  }
}
