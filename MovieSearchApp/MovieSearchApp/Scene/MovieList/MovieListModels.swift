//
//  MovieListModels.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 1/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

struct MovieList {
  struct GetSortList {
    struct Request {}
    struct Response {
      let page: Int
      let movies: [Movie]
    }
    struct ViewModel {
      let movies: [MovieModel]
    }
  }
  
  struct LoadMoreMovie {
    struct Request {
      let page: Int
      let searchText: String
    }
    struct Response {
      let result: Result<[Movie], APIError>
    }
    struct ViewModel {
      let result: Result<[MovieModel], APIError>
    }
  }
}
