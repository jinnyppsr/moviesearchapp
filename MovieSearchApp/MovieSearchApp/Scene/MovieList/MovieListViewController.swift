//
// MovieListViewController.swift
// MovieSearchApp
//
// Created by PRAPASIRI LERTKRIANGKRAIYING on 1/9/2563 BE.
// Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//
import UIKit

protocol MovieListViewControllerInterface: class {
  func displayMovieList(viewModel: MovieList.GetSortList.ViewModel)
  func displayLoadMore(viewModel: MovieList.LoadMoreMovie.ViewModel)
}

class MovieListViewController: UIViewController, MovieListViewControllerInterface {
  var interactor: MovieListInteractorInterface!
  var router: MovieListRouterInterface!

  @IBOutlet private weak var tableView: UITableView!
  var movieList: [MovieModel] = []
  var isLoading: Bool = false
  var isLoadMore: Bool = true
  var movieSelected: MovieModel?

  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }

  // MARK: - Configuration

  private func configure(viewController: MovieListViewController) {
    let router = MovieListRouter()
    router.viewController = viewController

    let presenter = MovieListPresenter()
    presenter.viewController = viewController

    let interactor = MovieListInteractor()
    interactor.presenter = presenter
    
    interactor.worker = MovieSearchWorker(store: MovieSearchStore(), storage: DeviceStorage())

    viewController.interactor = interactor
    viewController.router = router
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    registerNib()
    getSortedList()
  }

  // MARK: - Event handling
  
  private func registerNib() {
    let bundle = Bundle(for: MovieTableViewCell.self)
    let nib = UINib(nibName: "MovieTableViewCell", bundle: bundle)
    tableView.register(nib, forCellReuseIdentifier: "MovieTableViewCell")
    let loadBundle = Bundle(for: LoadingCell.self)
    let tableViewLoadingCellNib = UINib(nibName: "LoadingCell", bundle: loadBundle)
    tableView.register(tableViewLoadingCellNib, forCellReuseIdentifier: "LoadingCell")
  }
  
  private func loadMoreData() {
    if !self.isLoading {
      self.isLoading = true
      DispatchQueue.global().async {
        // Fake background loading task for 2 seconds
        sleep(2)
        // Download more data here
        DispatchQueue.main.async {
          self.tableView.reloadData()
          self.isLoading = false
        }
      }
    }
  }
  
  private func getSortedList() {
    let request = MovieList.GetSortList.Request()
    interactor.sortMovieList(request: request)
  }

  // MARK: - Display logic
  
  func displayMovieList(viewModel: MovieList.GetSortList.ViewModel) {
    movieList = viewModel.movies
    tableView.reloadData()
  }
  
  func displayLoadMore(viewModel: MovieList.LoadMoreMovie.ViewModel) {
    switch viewModel.result {
    case .success(let movies):
      if movies.count > 0 {
        var newIndexPaths = [IndexPath]()
        for rowPosition in 0..<movies.count {
          let newIndexPath = IndexPath(row: movieList.count + rowPosition, section: 0)
          newIndexPaths.append(newIndexPath)
        }
        movieList += movies
        tableView.insertRows(at: newIndexPaths, with: .automatic)
      } else {
        isLoadMore = false
      }
    case .failure:
      let alert = UIAlertController(title: "Oops!", message: "No Internet", preferredStyle: UIAlertController.Style.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
  }
}

extension MovieListViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      //Return the amount of items
      return movieList.count
    } else if section == 1 && isLoadMore {
      //Return the Loading cell
      return 1
    } else {
      //Return nothing
      return 0
    }
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let offsetY = scrollView.contentOffset.y
    let contentHeight = scrollView.contentSize.height
    if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading {
      loadMoreData()
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      guard let movieCell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as? MovieTableViewCell else {
        return UITableViewCell()
      }
      let viewModel = movieList[indexPath.row]
      movieCell.setUp(viewModel: viewModel)
      return movieCell
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
      cell.activityIndicator.startAnimating()
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return UITableView.automaticDimension
    } else {
      return 55 //Loading Cell height
    }
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    let lastElement = movieList.count - 1
    if indexPath.row == lastElement {
      if isLoadMore {
        let request = MovieList.LoadMoreMovie.Request(page: interactor.pageMore, searchText: interactor.searchText)
        interactor.loadMoreMovie(request: request)
      }
    }
  }
}

extension MovieListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let movieModel = movieList[indexPath.row]
    movieSelected = movieModel
    router.navigateToMovieDetail(movieModel: movieModel)
  }
}
