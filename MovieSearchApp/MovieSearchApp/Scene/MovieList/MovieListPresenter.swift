//
//  MovieListPresenter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 1/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieListPresenterInterface {
  func presentMovieList(response: MovieList.GetSortList.Response)
  func presentLoadMore(response: MovieList.LoadMoreMovie.Response)
}

class MovieListPresenter: MovieListPresenterInterface {
  weak var viewController: MovieListViewControllerInterface!

  // MARK: - Presentation logic
  
  func presentMovieList(response: MovieList.GetSortList.Response) {
    let movieModels = self.prepareData(movieList: response.movies)
    let viewModel = MovieList.GetSortList.ViewModel(movies: movieModels)
    viewController.displayMovieList(viewModel: viewModel)
  }
  
  func presentLoadMore(response: MovieList.LoadMoreMovie.Response) {
    switch response.result {
    case .success(let movies):
      let movieModels = self.prepareData(movieList: movies)
      let viewModel = MovieList.LoadMoreMovie.ViewModel(result: .success(movieModels))
      viewController.displayLoadMore(viewModel: viewModel)
    case .failure(let error):
      let viewModel = MovieList.LoadMoreMovie.ViewModel(result: .failure(error))
      viewController.displayLoadMore(viewModel: viewModel)
    }
  }
  
  private func prepareData(movieList: [Movie]) -> [MovieModel] {
    var movies: [MovieModel] = []
    let posterURL: String = "https://image.tmdb.org/t/p/w92"
    for item in movieList {
      if let path = item.posterPath {
        let posterPath: String = posterURL + path
        if let date = item.releaseDate {
          let movie: MovieModel = MovieModel(voteAverage: item.voteAverage,
                                             overView: item.overView,
                                             releaseDate: date,
                                             title: item.title,
                                             posterURL: posterPath)
          movies.append(movie)
        }
      }
    }
    return movies
  }
}
