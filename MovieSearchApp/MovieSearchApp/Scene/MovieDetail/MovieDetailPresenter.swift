//
//  MovieDetailPresenter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 2/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieDetailPresenterInterface {
  func presentMovieDetail(response: MovieDetail.ShowDetail.Response)
  func presentFavouriteStatus(response: MovieDetail.FavouriteMovie.Response)
  func presentButtonText(response: MovieDetail.ShowButtonText.Response)
}

class MovieDetailPresenter: MovieDetailPresenterInterface {
  weak var viewController: MovieDetailViewControllerInterface!

  // MARK: - Presentation logic
  
  func presentMovieDetail(response: MovieDetail.ShowDetail.Response) {
    let voteString: String = "Average votes: \(response.movie.voteAverage)"
    let viewModel = MovieDetail.ShowDetail.ViewModel(voteText: voteString, movie: response.movie)
    viewController.displayMovieDetail(viewModel: viewModel)
  }
  
  func presentFavouriteStatus(response: MovieDetail.FavouriteMovie.Response) {
    if response.favouriteStatus {
      let btnText: String = "Unfavourite"
      let viewModel = MovieDetail.FavouriteMovie.ViewModel(btnText: btnText)
      viewController.displayButtonText(viewModel: viewModel)
    } else {
      let btnText: String = "Favourite"
      let viewModel = MovieDetail.FavouriteMovie.ViewModel(btnText: btnText)
      viewController.displayButtonText(viewModel: viewModel)
    }
  }
  
  func presentButtonText(response: MovieDetail.ShowButtonText.Response) {
    let btnText: String = "Unfavourite"
    let viewModel = MovieDetail.ShowButtonText.ViewModel(btnText: btnText)
    viewController.displayButton(viewModel: viewModel)
  }
}
