//
//  MovieDetailInteractor.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 2/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieDetailInteractorInterface {
  func showMovieDetail(request: MovieDetail.ShowDetail.Request)
  func addFavouriteMovies(request: MovieDetail.FavouriteMovie.Request)
  func showButtonText(request: MovieDetail.ShowButtonText.Request)
  var movieSelected: MovieModel? { get set }
}

class MovieDetailInteractor: MovieDetailInteractorInterface {
  var presenter: MovieDetailPresenterInterface!
  var movieSelected: MovieModel?
  var worker: MovieSearchWorker?

  // MARK: - Business logic
  
  func showMovieDetail(request: MovieDetail.ShowDetail.Request) {
    if let movie = movieSelected {
      let response = MovieDetail.ShowDetail.Response(movie: movie)
      presenter.presentMovieDetail(response: response)
    }
  }
  
  func addFavouriteMovies(request: MovieDetail.FavouriteMovie.Request) {
    var favouriteModel: [MovieModel] = []
    if let favouriteList = worker?.getFavoriteMovies() {
      favouriteModel = favouriteList
      if favouriteModel.contains(where: { (item) -> Bool in
        item.title == request.favouriteMovie.title}) {
        guard let index = favouriteModel.firstIndex(where: { (item) -> Bool in
          request.favouriteMovie.title == item.title
        }) else { return }
        favouriteModel.remove(at: index)
        worker?.saveFavouriteMovies(favouriteMovies: favouriteModel)
        let response = MovieDetail.FavouriteMovie.Response(favouriteStatus: false)
        presenter.presentFavouriteStatus(response: response)
      } else {
        favouriteModel.append(request.favouriteMovie)
        worker?.saveFavouriteMovies(favouriteMovies: favouriteModel)
        let response = MovieDetail.FavouriteMovie.Response(favouriteStatus: true)
        presenter.presentFavouriteStatus(response: response)
      }
    }
  }
  
  func showButtonText(request: MovieDetail.ShowButtonText.Request) {
    if let movie = self.movieSelected {
      if let favouriteList = worker?.getFavoriteMovies() {
        if favouriteList.contains(where: { (item) -> Bool in
          item.title == movie.title}) {
          let response = MovieDetail.ShowButtonText.Response()
          presenter.presentButtonText(response: response)
        }
      }
    }
  }
}
