//
//  MovieDetailRouter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 2/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieDetailRouterInterface {
  func navigateToMovieSearch()
}

class MovieDetailRouter: MovieDetailRouterInterface {
  weak var viewController: MovieDetailViewController!

  // MARK: - Navigation
  
  func navigateToMovieSearch() {
    let destinationSB = UIStoryboard(name: "Main", bundle: nil)
    let destinationVC = destinationSB.instantiateViewController(withIdentifier: "MovieSearch") as! MovieSearchViewController
    viewController.navigationController?.pushViewController(destinationVC, animated: true)
  }
}
