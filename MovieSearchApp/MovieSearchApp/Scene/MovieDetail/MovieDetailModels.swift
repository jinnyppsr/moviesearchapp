//
//  MovieDetailModels.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 2/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

struct MovieDetail {
  struct ShowDetail {
    struct Request {}
    struct Response {
      let movie: MovieModel
    }
    struct ViewModel {
      let voteText: String
      let movie: MovieModel
    }
  }
  
  struct FavouriteMovie {
    struct Request {
      let favouriteMovie: MovieModel
    }
    struct Response {
      let favouriteStatus: Bool
    }
    struct ViewModel {
      let btnText: String
    }
  }
  
  struct ShowButtonText {
    struct Request {}
    struct Response {}
    struct ViewModel {
      let btnText: String
    }
  }
}
