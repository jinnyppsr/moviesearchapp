//
//  MovieDetailViewController.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 2/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit
import Kingfisher

protocol MovieDetailViewControllerInterface: class {
  func displayMovieDetail(viewModel: MovieDetail.ShowDetail.ViewModel)
  func displayButtonText(viewModel: MovieDetail.FavouriteMovie.ViewModel)
  func displayButton(viewModel: MovieDetail.ShowButtonText.ViewModel)
}

class MovieDetailViewController: UIViewController, MovieDetailViewControllerInterface {
  var interactor: MovieDetailInteractorInterface!
  var router: MovieDetailRouterInterface!
  var backToSearchBtn: UIBarButtonItem!
  var movie: MovieModel?

  @IBOutlet private weak var movieImage: UIImageView!
  @IBOutlet private weak var movieTitle: UILabel!
  @IBOutlet private weak var voteLabel: UILabel!
  @IBOutlet private weak var overViewLabel: UILabel!
  @IBOutlet private weak var favouriteBtn: UIButton!
  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }

  // MARK: - Configuration
  @IBAction func clickFavourite(_ sender: Any) {
    if let favouriteMovie = movie {
      let request = MovieDetail.FavouriteMovie.Request(favouriteMovie: favouriteMovie)
      interactor.addFavouriteMovies(request: request)
    }
    
    // call interactor.
  }
  
  private func configure(viewController: MovieDetailViewController) {
    let router = MovieDetailRouter()
    router.viewController = viewController

    let presenter = MovieDetailPresenter()
    presenter.viewController = viewController

    let interactor = MovieDetailInteractor()
    interactor.presenter = presenter
    interactor.worker = MovieSearchWorker(store: MovieSearchStore(), storage: DeviceStorage())

    viewController.interactor = interactor
    viewController.router = router
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    showBackToSearchBtn()
    showMovieDetail()
    showButtonText()
  }

  // MARK: - Event handling
  
  @objc func backToSearchBtnAction(_ sender: UIBarButtonItem) {
    self.navigationController?.popToRootViewController(animated: true)
  }
  
  private func showButtonText() {
    let request = MovieDetail.ShowButtonText.Request()
    interactor.showButtonText(request: request)
  }
  
  private func showBackToSearchBtn() {
    self.backToSearchBtn = UIBarButtonItem(title: "Back to Search", style: .done, target: self, action: #selector(backToSearchBtnAction(_:)))
    self.navigationItem.rightBarButtonItem = self.backToSearchBtn
  }

  private func showMovieDetail() {
    let request = MovieDetail.ShowDetail.Request()
    interactor.showMovieDetail(request: request)
  }

  // MARK: - Display logic

  func displayMovieDetail(viewModel: MovieDetail.ShowDetail.ViewModel) {
    movie = viewModel.movie
    let imageURL = URL(string: viewModel.movie.posterURL)
    movieImage.kf.setImage(with: imageURL)
    movieTitle.text = viewModel.movie.title
    voteLabel.text = viewModel.voteText
    overViewLabel.text = viewModel.movie.overView
  }
  
  func displayButtonText(viewModel: MovieDetail.FavouriteMovie.ViewModel) {
    favouriteBtn.setTitle(viewModel.btnText, for: .normal)
  }
  
  func displayButton(viewModel: MovieDetail.ShowButtonText.ViewModel) {
    favouriteBtn.setTitle(viewModel.btnText, for: .normal)
  }
}
