//
//  FavouriteListViewController.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 3/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol FavouriteListViewControllerInterface: class {
  func displayFavouriteList(viewModel: FavouriteList.GetList.ViewModel)
}

class FavouriteListViewController: UIViewController, FavouriteListViewControllerInterface {
  var interactor: FavouriteListInteractorInterface!
  var router: FavouriteListRouterInterface!
  
  var movieList: [MovieModel] = []
  var movieSelected: MovieModel?

  @IBOutlet private weak var favouriteTableView: UITableView!
  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }

  // MARK: - Configuration

  private func configure(viewController: FavouriteListViewController) {
    let router = FavouriteListRouter()
    router.viewController = viewController

    let presenter = FavouriteListPresenter()
    presenter.viewController = viewController

    let interactor = FavouriteListInteractor()
    interactor.presenter = presenter
    interactor.worker = MovieSearchWorker(store: MovieSearchStore(), storage: DeviceStorage())

    viewController.interactor = interactor
    viewController.router = router
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    registerNib()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    getFavouriteList()
  }

  // MARK: - Event handling

  private func registerNib() {
    let bundle = Bundle(for: MovieTableViewCell.self)
    let nib = UINib(nibName: "MovieTableViewCell", bundle: bundle)
    favouriteTableView.register(nib, forCellReuseIdentifier: "MovieTableViewCell")
  }
  
  private func getFavouriteList() {
    let request = FavouriteList.GetList.Request()
    interactor.getFavouriteList(request: request)
  }

  // MARK: - Display logic
  
  func displayFavouriteList(viewModel: FavouriteList.GetList.ViewModel) {
    movieList = viewModel.movies
    favouriteTableView.reloadData()
  }
}

extension FavouriteListViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return movieList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let movieCell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as? MovieTableViewCell else {
      return UITableViewCell()
    }
    let viewModel = movieList[indexPath.row]
    movieCell.setUp(viewModel: viewModel)
    return movieCell
  }
}

extension FavouriteListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let movieModel = movieList[indexPath.row]
    movieSelected = movieModel
    if let movieSelect = self.movieSelected {
      router.navigateToMovieDetail(movieModel: movieSelect)
    }
  }
}
