//
//  FavouriteListInteractor.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 3/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol FavouriteListInteractorInterface {
  func getFavouriteList(request: FavouriteList.GetList.Request)
}

class FavouriteListInteractor: FavouriteListInteractorInterface {
  var presenter: FavouriteListPresenterInterface!
  var worker: MovieSearchWorker?

  // MARK: - Business logic

  func getFavouriteList(request: FavouriteList.GetList.Request) {
    if let favourites = worker?.getFavoriteMovies() {
      let response = FavouriteList.GetList.Response(movies: favourites)
      presenter.presentFavouriteList(response: response)
    }
  }
}
