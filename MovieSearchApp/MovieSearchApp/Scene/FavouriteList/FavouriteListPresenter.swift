//
//  FavouriteListPresenter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 3/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol FavouriteListPresenterInterface {
  func presentFavouriteList(response: FavouriteList.GetList.Response)
}

class FavouriteListPresenter: FavouriteListPresenterInterface {
  weak var viewController: FavouriteListViewControllerInterface!

  // MARK: - Presentation logic
  func presentFavouriteList(response: FavouriteList.GetList.Response) {
    let viewModel = FavouriteList.GetList.ViewModel(movies: response.movies)
    viewController.displayFavouriteList(viewModel: viewModel)
  }
}
