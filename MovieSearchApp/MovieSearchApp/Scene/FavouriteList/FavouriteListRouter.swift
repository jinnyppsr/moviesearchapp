//
//  FavouriteListRouter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 3/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol FavouriteListRouterInterface {
  func navigateToMovieDetail(movieModel: MovieModel)
}

class FavouriteListRouter: FavouriteListRouterInterface {
  weak var viewController: FavouriteListViewController!

  // MARK: - Navigation

  func navigateToMovieDetail(movieModel: MovieModel) {
    let destinationSB = UIStoryboard(name: "MovieDetail", bundle: nil)
    let destinationVC = destinationSB.instantiateViewController(withIdentifier: "MovieDetail") as! MovieDetailViewController
    destinationVC.interactor.movieSelected = movieModel
    viewController.navigationController?.pushViewController(destinationVC, animated: true)
  }
}
