//
//  FavouriteListModels.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 3/9/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

struct FavouriteList {
  struct GetList {
    struct Request {}
    struct Response {
      let movies: [MovieModel]
    }
    struct ViewModel {
      let movies: [MovieModel]
    }
  }
}
