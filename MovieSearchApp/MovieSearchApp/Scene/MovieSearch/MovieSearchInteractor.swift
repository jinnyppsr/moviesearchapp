//
//  MovieSearchInteractor.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieSearchInteractorInterface {
  func searchMovies(request: MovieSearch.SearchMovies.Request)
  func loadHistory(request: MovieSearch.LoadHistory.Request)
  var movieResult: MovieResult? { get set }
  var searchText: String { get set }
}

class MovieSearchInteractor: MovieSearchInteractorInterface {
  var presenter: MovieSearchPresenterInterface!
  var worker: MovieSearchWorker?
  
  var movieResult: MovieResult?
  var searchTexts: [String] = []
  var searchText: String = ""
  var searchPath: String = ""

  // MARK: - Business logic
  
  func searchMovies(request: MovieSearch.SearchMovies.Request) {
    var allowedQueryParamAndKey = NSCharacterSet.urlQueryAllowed
    allowedQueryParamAndKey.remove(charactersIn: ";/?:@&=+$, ")
    if let editText = request.searchText.addingPercentEncoding(withAllowedCharacters: allowedQueryParamAndKey){
      searchPath = editText
    }
    worker?.getMoviesFromApi({ [weak self] (result) in
      guard let strongSelf = self else {
        return
      }
      switch result {
      case .success(let movieResult):
        strongSelf.movieResult = movieResult
        if var histories = self?.worker?.getHistory() {
          if !histories.contains(request.searchText) {
            histories.append(request.searchText)
            strongSelf.searchTexts = histories
            strongSelf.worker?.saveSearchTexts(searchTexts: histories)
          }
        }
        strongSelf.searchText = strongSelf.searchPath
        let response = MovieSearch.SearchMovies.Response(result: .success(strongSelf.searchTexts))
        strongSelf.presenter.presentMovieList(response: response)
      case .failure(let error):
        let response = MovieSearch.SearchMovies.Response(result: .failure(error))
        strongSelf.presenter.presentMovieList(response: response)
      }
      }, page: 1, textTitle: searchPath)
  }
  
  func loadHistory(request: MovieSearch.LoadHistory.Request) {
    if let histories = worker?.getHistory() {
      let response = MovieSearch.LoadHistory.Response(searchTexts: histories)
      self.presenter.presentHistory(response: response)
    }
  }
}
