//
//  MovieSearchPresenter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieSearchPresenterInterface {
  func presentMovieList(response: MovieSearch.SearchMovies.Response)
  func presentHistory(response: MovieSearch.LoadHistory.Response)
}

class MovieSearchPresenter: MovieSearchPresenterInterface {
  weak var viewController: MovieSearchViewControllerInterface!

  // MARK: - Presentation logic

  func presentMovieList(response: MovieSearch.SearchMovies.Response) {
    let viewModel = MovieSearch.SearchMovies.ViewModel(result: response.result)
    viewController.displayMovieList(viewModel: viewModel)
  }
  
  func presentHistory(response: MovieSearch.LoadHistory.Response) {
    let viewModel = MovieSearch.LoadHistory.ViewModel(searchTexts: response.searchTexts)
    viewController.displayHistory(viewModel: viewModel)
  }
}
