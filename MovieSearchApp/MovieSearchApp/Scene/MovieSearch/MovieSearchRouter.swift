//
//  MovieSearchRouter.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieSearchRouterInterface {
  func navigateToMovieListResult()
  func navigateToFavouriteList()
}

class MovieSearchRouter: MovieSearchRouterInterface {
  weak var viewController: MovieSearchViewController!

  // MARK: - Navigation
  
  func navigateToMovieListResult() {
    let destinationSB = UIStoryboard(name: "MovieList", bundle: nil)
    let destinationVC = destinationSB.instantiateViewController(withIdentifier: "MovieListView") as! MovieListViewController
    destinationVC.interactor.movieResult = viewController.interactor.movieResult
    destinationVC.interactor.searchText = viewController.interactor.searchText
    viewController.navigationController?.pushViewController(destinationVC, animated: true)
  }
  
  func navigateToFavouriteList() {
    let destinationSB = UIStoryboard(name: "FavouriteList", bundle: nil)
    let destinationVC = destinationSB.instantiateViewController(withIdentifier: "FavouriteList") as! FavouriteListViewController
    viewController.navigationController?.pushViewController(destinationVC, animated: true)
  }

}
