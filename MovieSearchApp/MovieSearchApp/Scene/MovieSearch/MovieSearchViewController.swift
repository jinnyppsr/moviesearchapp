//
//  MovieSearchViewController.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

protocol MovieSearchViewControllerInterface: class {
  func displayMovieList(viewModel: MovieSearch.SearchMovies.ViewModel)
  func displayHistory(viewModel: MovieSearch.LoadHistory.ViewModel)
}

class MovieSearchViewController: UIViewController, MovieSearchViewControllerInterface {
  var interactor: MovieSearchInteractorInterface!
  var router: MovieSearchRouterInterface!

  @IBOutlet private weak var tableView: UITableView!
  
  var searchingText: String = ""
  var searchingTexts: [String] = []

  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }

  // MARK: - Configuration

  private func configure(viewController: MovieSearchViewController) {
    let router = MovieSearchRouter()
    router.viewController = viewController

    let presenter = MovieSearchPresenter()
    presenter.viewController = viewController

    let interactor = MovieSearchInteractor()
    interactor.presenter = presenter
    interactor.worker = MovieSearchWorker(store: MovieSearchStore(), storage: DeviceStorage())

    viewController.interactor = interactor
    viewController.router = router
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    registerNib()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    loadSearchHistory()
  }

  // MARK: - Event handling
  private func registerNib() {
    let bundle = Bundle(for: SearchHistoryTableViewCell.self)
    let nib = UINib(nibName: "SearchHistoryTableViewCell", bundle: bundle)
    tableView.register(nib, forCellReuseIdentifier: "SearchHistoryTableViewCell")
  }
  
  private func loadSearchHistory() {
    let request = MovieSearch.LoadHistory.Request()
    interactor.loadHistory(request: request)
  }
  
  @IBAction func clickFavouriteBtn(_ sender: Any) {
    router.navigateToFavouriteList()
  }
  
  // MARK: - Display logic

  func displayMovieList(viewModel: MovieSearch.SearchMovies.ViewModel) {
    switch viewModel.result {
    case .success(let searchTexts):
      searchingTexts = searchTexts
      router.navigateToMovieListResult()
    case .failure:
      let alert = UIAlertController(title: "Oops!", message: "No Internet", preferredStyle: UIAlertController.Style.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
  }
  
  func displayHistory(viewModel: MovieSearch.LoadHistory.ViewModel) {
    searchingTexts = viewModel.searchTexts
    tableView.reloadData()
  }
}

extension MovieSearchViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return searchingTexts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let historyCell = tableView.dequeueReusableCell(withIdentifier: "SearchHistoryTableViewCell", for: indexPath) as? SearchHistoryTableViewCell else {
      return UITableViewCell()
    }
    let text = searchingTexts[indexPath.row]
    historyCell.setUp(text: text)
    return historyCell
  }
}

extension MovieSearchViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    searchingText = searchingTexts[indexPath.row]
    let request = MovieSearch.SearchMovies.Request(searchText: searchingText)
    interactor.searchMovies(request: request)
  }
}

extension MovieSearchViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {    self.searchingText = searchText
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.text = ""
    tableView.reloadData()
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    // call api and search
    let request = MovieSearch.SearchMovies.Request(searchText: self.searchingText)
    interactor.searchMovies(request: request)
  }

}

