//
//  MovieSearchModels.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 31/8/2563 BE.
//  Copyright (c) 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

struct MovieSearch {
  struct SearchMovies {
    struct Request {
      let searchText: String
    }
    struct Response {
      let result: Result<[String], APIError>
    }
    struct ViewModel {
      let result: Result<[String], APIError>
    }
  }
  
  struct LoadHistory {
    struct Request {}
    struct Response {
      let searchTexts: [String]
    }
    struct ViewModel {
      let searchTexts: [String]
    }
  }
}
