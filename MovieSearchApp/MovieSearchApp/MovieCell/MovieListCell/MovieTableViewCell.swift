//
//  MovieTableViewCell.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 1/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit
import Kingfisher

class MovieTableViewCell: UITableViewCell {
  
  @IBOutlet private weak var posterImage: UIImageView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var dateLabel: UILabel!
  @IBOutlet private weak var overViewLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
      // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func setUp(viewModel: MovieModel) {
    let imageURL = URL(string: viewModel.posterURL)
    posterImage.kf.setImage(with: imageURL)
    titleLabel.text = viewModel.title
    dateLabel.text = viewModel.releaseDate
    overViewLabel.text = viewModel.overView
  }
}
