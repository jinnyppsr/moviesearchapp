//
//  SearchHistoryTableViewCell.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 2/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import UIKit

class SearchHistoryTableViewCell: UITableViewCell {
      
  @IBOutlet private weak var searchLabel: UILabel!
  override func awakeFromNib() {
    super.awakeFromNib()
        // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func setUp(text: String) {
    searchLabel.text = text
  }
}
