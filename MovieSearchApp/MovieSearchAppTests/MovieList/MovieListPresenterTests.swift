//
//  Movie.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class MovieListPresenterTests: XCTestCase {
  var presenter: MovieListPresenter!
  
  override func setUp() {
    super.setUp()
    presenter = MovieListPresenter()
  }
  
  class OutputSpy: MovieListViewControllerInterface {
    var displayMovieListCalled: Bool = false
    var displayMovieListViewModel: MovieList.GetSortList.ViewModel?
    var displayLoadMoreCalled: Bool = false
    var displayLoadMoreViewModel: MovieList.LoadMoreMovie.ViewModel?
    
    func displayMovieList(viewModel: MovieList.GetSortList.ViewModel) {
      displayMovieListCalled = true
      displayMovieListViewModel = viewModel
    }
    
    func displayLoadMore(viewModel: MovieList.LoadMoreMovie.ViewModel) {
      displayLoadMoreCalled = true
      displayLoadMoreViewModel = viewModel
    }
  }
  
  func testPresentMovieList() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let movies: [Movie] = [
      Movie(voteAverage: 7.1,
            overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2005-10-18",
            title: "Batman & Mr. Freeze: SubZero",
            posterURL: "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg"),
      Movie(voteAverage: 6.6,
            overView: "Gotham City is terrorized not only by recent escapees Joker and Penguin, but by the original creature of the night, Dracula!",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2015-10-18",
            title: "Batman: Gotham Knight",
            posterURL: "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg")
    ]
    let response = MovieList.GetSortList.Response(page: 1, movies: movies)
    
    // When
    presenter.presentMovieList(response: response)
    
    // Then
    if let viewModel = outputSpy.displayMovieListViewModel?.movies {
      XCTAssertEqual(viewModel[0].posterURL, "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg")
      XCTAssertEqual(viewModel[1].posterURL, "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg")
      XCTAssert(outputSpy.displayMovieListCalled, "presentMovieList should call displayMovieList")
    }
  }
  
  func testPresentLoadMoreSuccess() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let movies: [Movie] = [
      Movie(voteAverage: 7.1,
            overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2005-10-18",
            title: "Batman & Mr. Freeze: SubZero",
            posterURL: "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg"),
      Movie(voteAverage: 6.6,
            overView: "Gotham City is terrorized not only by recent escapees Joker and Penguin, but by the original creature of the night, Dracula!",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2015-10-18",
            title: "Batman: Gotham Knight",
            posterURL: "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg")
    ]
    let response = MovieList.LoadMoreMovie.Response(result: .success(movies))
    
    // When
    presenter.presentLoadMore(response: response)
    
    // Then
    if let viewModel = outputSpy.displayLoadMoreViewModel?.result {
      switch viewModel {
      case .success(let movieModels):
        XCTAssertEqual(movieModels[0].posterURL, "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg")
        XCTAssertEqual(movieModels[1].posterURL, "https://image.tmdb.org/t/p/w92/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg")
        XCTAssert(outputSpy.displayLoadMoreCalled, "presentMovieList should call displayLoadMore")
      default:
        break
      }
    } else {
      XCTFail("Fail Response")
    }
  }
  
  func testPresentLoadMoreFail() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let response = MovieList.LoadMoreMovie.Response(result: .failure(.invalidJSON))
    
    // When
    presenter.presentLoadMore(response: response)
    
    // Then
    if let viewModel = outputSpy.displayLoadMoreViewModel?.result {
      switch viewModel {
      case .failure(let error):
        XCTAssertEqual(error.localizedDescription, "The operation couldn’t be completed. (MovieSearchApp.APIError error 0.)")
        XCTAssert(outputSpy.displayLoadMoreCalled, "presentMovieList should call displayLoadMore")
      default:
        break
      }
    } else {
      XCTFail("Fail Response")
    }
  }
}
