//
//  MovieListInteractorTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class MovieListInteractorTestSuite: XCTestCase {
  var interactor: MovieListInteractor!
  
  override func setUp() {
    super.setUp()
    interactor = MovieListInteractor()
  }
  
  class MovieListWorkerSpy: MovieSearchWorker {
    var getMoviesFromApiCalled: Bool = false
    var isFailGetMovieFromApi: Bool = false
    
    override func getMoviesFromApi(_ completion: @escaping (Result<MovieResult, APIError>) -> Void, page: Int, textTitle: String) {
      getMoviesFromApiCalled = true
      if !self.isFailGetMovieFromApi {
        super.getMoviesFromApi(completion, page: page, textTitle: textTitle)
      } else {
        completion(Result.failure(APIError.invalidJSON))
      }
    }
  }
  
  class MovieListPresenterSpy: MovieListPresenterInterface {
    var presentMovieListCalled: Bool = false
    var presentMovieListResponse: MovieList.GetSortList.Response?
    var presentLoadMoreCalled: Bool = false
    var presentLoadMoreResponse: MovieList.LoadMoreMovie.Response?
    
    func presentMovieList(response: MovieList.GetSortList.Response) {
      presentMovieListCalled = true
      presentMovieListResponse = response
    }
    
    func presentLoadMore(response: MovieList.LoadMoreMovie.Response) {
      presentLoadMoreCalled = true
      presentLoadMoreResponse = response
    }
  }
  
  func testLoadMoreMovieSuccess() {
    // Given
    let workerSpy = MovieListWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    interactor.worker = workerSpy
    let outputSpy = MovieListPresenterSpy()
    interactor.presenter = outputSpy
    let request = MovieList.LoadMoreMovie.Request(page: 1, searchText: "Batman: Gotham Knight")
    workerSpy.isFailGetMovieFromApi = false
    
    // When
    interactor.loadMoreMovie(request: request)
    
    // Then
    XCTAssert(workerSpy.getMoviesFromApiCalled, "loadMoreMovie should call getMoviesFromApi")
    if let response = outputSpy.presentLoadMoreResponse?.result {
      switch response {
      case .success(let movies):
        XCTAssert(outputSpy.presentLoadMoreCalled, "loadMoreMovie should call presentLoadMore")
        XCTAssertEqual(movies[0].title, "Batman: Gotham Knight")
        XCTAssertEqual(movies[0].releaseDate, "2015-10-18")
        XCTAssertEqual(movies[1].releaseDate, "2005-10-18")
      default:
        break
      }
    } else {
      XCTFail("Fail response")
    }
  }
  
  func testLoadMoreMovieFail() {
    // Given
    let workerSpy = MovieListWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    interactor.worker = workerSpy
    let outputSpy = MovieListPresenterSpy()
    interactor.presenter = outputSpy
    let request = MovieList.LoadMoreMovie.Request(page: 1, searchText: "Batman: Gotham Knight")
    workerSpy.isFailGetMovieFromApi = true
    
    // When
    interactor.loadMoreMovie(request: request)
    
    // Then
    XCTAssert(workerSpy.getMoviesFromApiCalled, "loadMoreMovie should call getMoviesFromApi")
    if let response = outputSpy.presentLoadMoreResponse?.result {
      switch response {
      case .failure(let error):
        switch error {
        case .invalidJSON:
          XCTAssert(outputSpy.presentLoadMoreCalled, "loadMoreMovie should call presentLoadMore")
          XCTAssertEqual(error.localizedDescription, "The operation couldn’t be completed. (MovieSearchApp.APIError error 0.)")
        default:
          break
        }
      default:
        break
      }
    } else {
      XCTFail("Fail response")
    }
  }
  
  func testSortMovieList() {
    // Given
    let outputSpy = MovieListPresenterSpy()
    interactor.presenter = outputSpy
    let movies: [Movie] = [
      Movie(voteAverage: 7.1,
            overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2005-10-18",
            title: "Batman & Mr. Freeze: SubZero",
            posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg"),
      Movie(voteAverage: 6.6,
            overView: "Gotham City is terrorized not only by recent escapees Joker and Penguin, but by the original creature of the night, Dracula!",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2015-10-18",
            title: "Batman: Gotham Knight",
            posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    ]
    let movieResult = MovieResult(movies: movies, page: 1, totalPages: 2)
    interactor.movieResult = movieResult
    let request = MovieList.GetSortList.Request()

    // When
    interactor.sortMovieList(request: request)
    
    // Then
    XCTAssert(outputSpy.presentMovieListCalled, "sortMovieList should call presentMovieList")
    if let response = outputSpy.presentMovieListResponse?.movies {
      XCTAssertEqual(response[0].title, "Batman: Gotham Knight")
      XCTAssertEqual(response[0].releaseDate, "2015-10-18")
      XCTAssertEqual(response[1].releaseDate, "2005-10-18")
    }
  }
}
