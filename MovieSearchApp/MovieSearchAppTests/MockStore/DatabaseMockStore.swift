//
//  DatabaseMockStore.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import Foundation

class DatabaseMockStore: DeviceStorageProtocol {
  func insertSearchText(text: String) {
    
  }
  
  func saveSearchTextList(textList: [String]) {
    
  }
  
  func getSearchText() -> [String] {
    let textList: [String] = ["Batman & Mr. Freeze: SubZero", "Batman vs. Teenage Mutant Ninja Turtles"]
    return textList
  }
  
  func saveFavouriteList(favouriteList: [MovieModel]) {
    
  }
  
  func getFavouriteMovie() -> [MovieModel] {
    let movie1: MovieModel = MovieModel(voteAverage: 7.1,
                                       overView: "Batman, Batgirl and Robin forge an alliance with the Teenage Mutant Ninja Turtles to fight against the Turtles' sworn enemy, The Shredder, who has apparently teamed up with Ra's Al Ghul and The League of Assassins.",
                                       releaseDate: "2019-03-31",
                                       title: "Batman vs. Teenage Mutant Ninja Turtles",
                                       posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let movie2: MovieModel = MovieModel(voteAverage: 7,
                                        overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
                                        releaseDate: "1998-02-11",
                                        title: "Batman & Mr. Freeze: SubZero",
                                        posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let movieList: [MovieModel] = [movie1, movie2]
    return movieList
  }
  
  
}
