//
//  DatabaseMockStore.swift
//  MovieSearchApp
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

import Foundation

class MoviesMockStore: MovieSearchStoreProtocol {
  func getMovieList(page: Int, textTitle: String, completion: @escaping (Result<MovieResult, APIError>) -> Void) {
    let movies: [Movie] = [
      Movie(voteAverage: 7.1,
            overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2005-10-18",
            title: "Batman & Mr. Freeze: SubZero",
            posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg"),
      Movie(voteAverage: 6.6,
            overView: "Gotham City is terrorized not only by recent escapees Joker and Penguin, but by the original creature of the night, Dracula!",
            posterPath: "/f3xUrqo7yEiU0guk2Ua3Znqiw6S.jpg",
            releaseDate: "2015-10-18",
            title: "Batman: Gotham Knight",
            posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    ]
    let movieResult = MovieResult(movies: movies, page: 1, totalPages: 2)
    completion(Result.success(movieResult))
  }
}
