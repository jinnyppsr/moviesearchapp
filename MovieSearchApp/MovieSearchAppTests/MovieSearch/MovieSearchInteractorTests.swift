//
//  MovieSearchInteractorTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class MovieSearchTestSuite: XCTestCase {
  var movieSearchInteractor: MovieSearchInteractor!
  
  override func setUp() {
    super.setUp()
    movieSearchInteractor = MovieSearchInteractor()
  }
  
  class MovieSearchWorkerSpy: MovieSearchWorker {
    var getMoviesFromApiCalled: Bool = false
    var isFailGetMovieFromApi: Bool = false
    var getHistoryCalled: Bool = false
    var saveSearchTextsCalled: Bool = false
    
    var response: MovieResult?
    var responseFail: APIError?
    
    override func getMoviesFromApi(_ completion: @escaping (Result<MovieResult, APIError>) -> Void, page: Int, textTitle: String) {
      getMoviesFromApiCalled = true
      if !self.isFailGetMovieFromApi {
        super.getMoviesFromApi(completion, page: page, textTitle: textTitle)
      } else {
        completion(Result.failure(APIError.invalidJSON))
      }
    }
    
    override func getHistory() -> [String] {
      getHistoryCalled = true
      return super.getHistory()
    }
    
    override func saveSearchTexts(searchTexts: [String]) {
      saveSearchTextsCalled = true
      return super.saveSearchTexts(searchTexts: searchTexts)
    }
  }
  
  class MovieSearchPresenterSpy: MovieSearchPresenterInterface {
    var presentMovieListCalled: Bool = false
    var presentMovieListResponse: MovieSearch.SearchMovies.Response?
    var presentHistoryCalled: Bool = false
    var presentHistoryResponse: MovieSearch.LoadHistory.Response?
    
    func presentMovieList(response: MovieSearch.SearchMovies.Response) {
      presentMovieListCalled = true
      presentMovieListResponse = response
    }
    
    func presentHistory(response: MovieSearch.LoadHistory.Response) {
      presentHistoryCalled = true
      presentHistoryResponse = response
    }
  }
  
  func testSearchMovieSuccess() {
    // Given
    let workerSpy = MovieSearchWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    movieSearchInteractor.worker = workerSpy
    let outputSpy = MovieSearchPresenterSpy()
    movieSearchInteractor.presenter = outputSpy
    let request = MovieSearch.SearchMovies.Request(searchText: "Batman: Under the Red Hood")
    workerSpy.isFailGetMovieFromApi = false
    // When
    movieSearchInteractor.searchMovies(request: request)
    
    // Then
    XCTAssert(workerSpy.getMoviesFromApiCalled, "searchMovie should getMoviesFromApi")
    XCTAssert(workerSpy.getHistoryCalled, "searchMovie should call getHistory")
    XCTAssert(workerSpy.saveSearchTextsCalled, "searchMovie should call saveSearchTexts")
    XCTAssert(outputSpy.presentMovieListCalled, "searchMovie should call presentMovieList")
    if let response = outputSpy.presentMovieListResponse?.result {
      switch response {
      case .success(let data):
        XCTAssertEqual(data.first, "Batman & Mr. Freeze: SubZero")
      default:
        break
      }
    } else {
      XCTFail("Fail response")
    }
  }
  
  func testSearchMovieFail() {
    // Given
    let workerSpy = MovieSearchWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    movieSearchInteractor.worker = workerSpy
    let outputSpy = MovieSearchPresenterSpy()
    movieSearchInteractor.presenter = outputSpy
    let request = MovieSearch.SearchMovies.Request(searchText: "Batman & Mr. Freeze: SubZero")
    workerSpy.isFailGetMovieFromApi = true
    // When
    movieSearchInteractor.searchMovies(request: request)
    
    // Then
    XCTAssert(workerSpy.getMoviesFromApiCalled, "searchMovie should getMoviesFromApi")
    XCTAssert(outputSpy.presentMovieListCalled, "searchMovie should call presentMovieList")
    if let response = outputSpy.presentMovieListResponse?.result {
      switch response {
      case .failure(let error):
        switch error {
        case .invalidJSON:
          XCTAssertEqual(error.localizedDescription, "The operation couldn’t be completed. (MovieSearchApp.APIError error 0.)")
        default:
          break
        }
      default:
        break
      }
    } else {
      XCTFail("Fail response")
    }
  }
  
  func testLoadHistory() {
    // Given
    let workerSpy = MovieSearchWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    movieSearchInteractor.worker = workerSpy
    let outputSpy = MovieSearchPresenterSpy()
    movieSearchInteractor.presenter = outputSpy
    let request = MovieSearch.LoadHistory.Request()
    
    // When
    movieSearchInteractor.loadHistory(request: request)
    
    // Then
    XCTAssert(workerSpy.getHistoryCalled, "searchMovie should call getHistory")
    XCTAssert(outputSpy.presentHistoryCalled, "searchMovie should call presentHistory")
    print("=======================")
    print(outputSpy.presentHistoryResponse?.searchTexts)
    if let response = outputSpy.presentHistoryResponse?.searchTexts {
      XCTAssertEqual(response.last, "Batman vs. Teenage Mutant Ninja Turtles")
    } else {
      XCTFail("Fail response")
    }
  }
}
