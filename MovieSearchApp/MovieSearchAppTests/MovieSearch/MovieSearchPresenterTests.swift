//
//  MovieSearchPresenterTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class MovieSearchPresenterTests: XCTestCase {
  var presenter: MovieSearchPresenter!
  
  override func setUp() {
    super.setUp()
    presenter = MovieSearchPresenter()
  }
  
  class OutputSpy: MovieSearchViewControllerInterface {
    var displayMovieListCalled: Bool = false
    var displayMovieListViewModel: MovieSearch.SearchMovies.ViewModel?
    var displayHistoryCalled: Bool = false
    var displayHistoryViewModel: MovieSearch.LoadHistory.ViewModel?
    
    func displayMovieList(viewModel: MovieSearch.SearchMovies.ViewModel) {
      displayMovieListCalled = true
      displayMovieListViewModel = viewModel
    }
    
    func displayHistory(viewModel: MovieSearch.LoadHistory.ViewModel) {
      displayHistoryCalled = true
    }
  }
  
  func testPresentMovieList() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let list: [String] = ["Batman vs. Teenage Mutant Ninja Turtles", "Batman & Mr. Freeze: SubZero"]
    let response = MovieSearch.SearchMovies.Response(result: .success(list))
    
    // When
    presenter.presentMovieList(response: response)
    
    // Then
    XCTAssert(outputSpy.displayMovieListCalled, "presentMovieList should call displayMovieList")
    if let viewModel = outputSpy.displayMovieListViewModel?.result {
      switch viewModel {
      case .success(let data):
        XCTAssertEqual(data.first, "Batman vs. Teenage Mutant Ninja Turtles")
      default:
        break
      }
    }
  }
  
  func testPresentHistory() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let list: [String] = ["Batman vs. Teenage Mutant Ninja Turtles", "Batman & Mr. Freeze: SubZero"]
    let response = MovieSearch.LoadHistory.Response(searchTexts: list)
    
    // When
    presenter.presentHistory(response: response)
    
    // Then
    XCTAssert(outputSpy.displayHistoryCalled, "presentMovieList should call displayHistory")
    if let viewModel = outputSpy.displayHistoryViewModel?.searchTexts {
      XCTAssertEqual(viewModel.first, "Batman vs. Teenage Mutant Ninja Turtles")
    }
  }
}
