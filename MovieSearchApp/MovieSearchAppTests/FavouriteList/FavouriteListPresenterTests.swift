//
//  FavouriteListPresenterTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class FavouriteListPresenterTests: XCTestCase {
  var presenter: FavouriteListPresenter!
  
  override func setUp() {
    super.setUp()
    presenter = FavouriteListPresenter()
  }
  
  class OutputSpy: FavouriteListViewControllerInterface {
    var displayFavouriteListCalled: Bool = false
    var displayFavouriteListViewModel: FavouriteList.GetList.ViewModel?
    
    func displayFavouriteList(viewModel: FavouriteList.GetList.ViewModel) {
      displayFavouriteListCalled = true
      displayFavouriteListViewModel = viewModel
    }
  }
  
  func testPresentFavouriteList() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let movie1: MovieModel = MovieModel(voteAverage: 7.1,
                                        overView: "Batman, Batgirl and Robin forge an alliance with the Teenage Mutant Ninja Turtles to fight against the Turtles' sworn enemy, The Shredder, who has apparently teamed up with Ra's Al Ghul and The League of Assassins.",
                                        releaseDate: "2019-03-31",
                                        title: "Batman vs. Teenage Mutant Ninja Turtles",
                                        posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let movie2: MovieModel = MovieModel(voteAverage: 7,
                                        overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
                                        releaseDate: "1998-02-11",
                                        title: "Batman & Mr. Freeze: SubZero",
                                        posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let movieList: [MovieModel] = [movie1, movie2]
    let response = FavouriteList.GetList.Response(movies: movieList)
    
    // When
    presenter.presentFavouriteList(response: response)
    
    // Then
    if let viewModel = outputSpy.displayFavouriteListViewModel?.movies {
      XCTAssertEqual(viewModel.first?.title, "Batman vs. Teenage Mutant Ninja Turtles")
      XCTAssertEqual(viewModel.first?.releaseDate, "2019-03-31")
    }
  }
}
