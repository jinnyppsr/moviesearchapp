//
//  FavouriteListInteractorTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class FavouriteListInteractorTestSuite: XCTestCase {
  var interactor: FavouriteListInteractor!
  
  override func setUp() {
    super.setUp()
    interactor = FavouriteListInteractor()
  }
  
  class FavouriteListWorkerSpy: MovieSearchWorker {
    var getFavoriteMoviesCalled: Bool = false
    
    override func getFavoriteMovies() -> [MovieModel] {
      getFavoriteMoviesCalled = true
      return super.getFavoriteMovies()
    }
  }
  
  class FavouriteListPresenterSpy: FavouriteListPresenterInterface {
    var presentFavouriteListCalled: Bool = false
    var presentFavouriteListResponse: FavouriteList.GetList.Response?
    
    func presentFavouriteList(response: FavouriteList.GetList.Response) {
      presentFavouriteListCalled = true
      presentFavouriteListResponse = response
    }
  }
  
  func testGetFavouriteList() {
    // Given
    let workerSpy = FavouriteListWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    interactor.worker = workerSpy
    let outputSpy = FavouriteListPresenterSpy()
    interactor.presenter = outputSpy
    let request = FavouriteList.GetList.Request()
    
    // When
    interactor.getFavouriteList(request: request)
    
    // Then
    XCTAssert(workerSpy.getFavoriteMoviesCalled, "showMovieDetail should call getFavoriteMovies")
    if let response = outputSpy.presentFavouriteListResponse?.movies {
      XCTAssertEqual(response.first?.title, "Batman vs. Teenage Mutant Ninja Turtles")
      XCTAssertEqual(response.first?.releaseDate, "2019-03-31")
    }
  }
}
