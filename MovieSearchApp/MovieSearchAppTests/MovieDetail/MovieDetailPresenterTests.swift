//
//  MovieDetailPresenterTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class MovieDetailPresenterTests: XCTestCase {
  var presenter: MovieDetailPresenter!
  
  override func setUp() {
    super.setUp()
    presenter = MovieDetailPresenter()
  }
  
  class OutputSpy: MovieDetailViewControllerInterface {
    var displayMovieDetailCalled: Bool = false
    var displayMovieDetailViewModel: MovieDetail.ShowDetail.ViewModel?
    var displayButtonTextCalled: Bool = false
    var displayButtonTextViewModel: MovieDetail.FavouriteMovie.ViewModel?
    var displayButtonCalled: Bool = false
    var displayButtonViewModel: MovieDetail.ShowButtonText.ViewModel?
    
    func displayMovieDetail(viewModel: MovieDetail.ShowDetail.ViewModel) {
      displayMovieDetailCalled = true
      displayMovieDetailViewModel = viewModel
    }
    
    func displayButtonText(viewModel: MovieDetail.FavouriteMovie.ViewModel) {
      displayButtonTextCalled = true
      displayButtonTextViewModel = viewModel
    }
    
    func displayButton(viewModel: MovieDetail.ShowButtonText.ViewModel) {
      displayButtonCalled = true
      displayButtonViewModel = viewModel
    }
  }
  
  func testPresentMovieDetail() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let movie = MovieModel(voteAverage: 7.1,
                           overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
                           releaseDate: "2005-10-18",
                           title: "Batman & Mr. Freeze: SubZero",
                           posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let response = MovieDetail.ShowDetail.Response(movie: movie)
    
    // When
    presenter.presentMovieDetail(response: response)
    
    // Then
    if let viewModel = outputSpy.displayMovieDetailViewModel {
      XCTAssertEqual(viewModel.voteText, "Average votes: 7.1")
      XCTAssertEqual(viewModel.movie.title, "Batman & Mr. Freeze: SubZero")
    }
  }
  
  func testPresentFavouriteStatusTrue() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let response = MovieDetail.FavouriteMovie.Response(favouriteStatus: true)
    
    // When
    presenter.presentFavouriteStatus(response: response)
    
    // Then
    if let viewModel = outputSpy.displayButtonTextViewModel?.btnText {
      XCTAssertEqual(viewModel, "Unfavourite")
    }
  }
  
  func testPresentFavouriteStatusFalse() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let response = MovieDetail.FavouriteMovie.Response(favouriteStatus: false)
    
    // When
    presenter.presentFavouriteStatus(response: response)
    
    // Then
    if let viewModel = outputSpy.displayButtonTextViewModel?.btnText {
      XCTAssertEqual(viewModel, "Favourite")
    }
  }
  
  func testPresentButtonText() {
    // Given
    let outputSpy = OutputSpy()
    presenter.viewController = outputSpy
    let response = MovieDetail.ShowButtonText.Response()
    
    // When
    presenter.presentButtonText(response: response)
    
    // Then
    if let viewModel = outputSpy.displayButtonViewModel?.btnText {
      XCTAssertEqual(viewModel, "Unfavourite")
    }
  }
}
