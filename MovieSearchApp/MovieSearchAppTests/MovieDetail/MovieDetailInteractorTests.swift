//
//  MovieDetailInteractorTests.swift
//  MovieSearchAppTests
//
//  Created by PRAPASIRI LERTKRIANGKRAIYING on 9/9/2563 BE.
//  Copyright © 2563 PRAPASIRI LERTKRIANGKRAIYING. All rights reserved.
//

@testable import MovieSearchApp
import Foundation
import XCTest

class MovieDetailInteractorTestSuite: XCTestCase {
  var interactor: MovieDetailInteractor!
  
  override func setUp() {
    super.setUp()
    interactor = MovieDetailInteractor()
  }
  
  class MovieDetailWorkerSpy: MovieSearchWorker {
    var getFavoriteMoviesCalled: Bool = false
    var saveFavouriteMoviesCalled: Bool = false
    
    override func getFavoriteMovies() -> [MovieModel] {
      getFavoriteMoviesCalled = true
      return super.getFavoriteMovies()
    }
    
    override func saveFavouriteMovies(favouriteMovies: [MovieModel]) {
      saveFavouriteMoviesCalled = true
    }
  }
  
  class MovieDetailPresenterSpy: MovieDetailPresenterInterface {
    var presentMovieDetailCalled: Bool = false
    var presentMovieDetailResponse: MovieDetail.ShowDetail.Response?
    var presentFavouriteStatusCalled: Bool = false
    var presentFavouriteStatusResponse: MovieDetail.FavouriteMovie.Response?
    var presentButtonTextCalled: Bool = false
    var presentButtonTextResponse: MovieDetail.ShowButtonText.Response?
    
    func presentMovieDetail(response: MovieDetail.ShowDetail.Response) {
      presentMovieDetailCalled = true
      presentMovieDetailResponse = response
    }
    
    func presentFavouriteStatus(response: MovieDetail.FavouriteMovie.Response) {
      presentFavouriteStatusCalled = true
      presentFavouriteStatusResponse = response
    }
    
    func presentButtonText(response: MovieDetail.ShowButtonText.Response) {
      presentButtonTextCalled = true
      presentButtonTextResponse = response
    }
  }
  
  func testShowMovieDetail() {
    // Given
    let outputSpy = MovieDetailPresenterSpy()
    interactor.presenter = outputSpy
    let movieSelected = MovieModel(voteAverage: 7.1,
                                   overView: "When a desperate Mr. Freeze, to save his dying wife, kidnaps Barbara Gordon (Batgirl) as an involuntary organ donor, Batman and Robin must find her before the operation can begin.",
                                   releaseDate: "2005-10-18",
                                   title: "Batman & Mr. Freeze: SubZero",
                                   posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    interactor.movieSelected = movieSelected
    let request = MovieDetail.ShowDetail.Request()
    
    // When
    interactor.showMovieDetail(request: request)
    
    // Then
    if let response = outputSpy.presentMovieDetailResponse?.movie {
      XCTAssertEqual(response.title, "Batman & Mr. Freeze: SubZero")
      XCTAssertEqual(response.voteAverage, 7.1)
      XCTAssert(outputSpy.presentMovieDetailCalled, "showMovieDetail should call presentMovieDetail")
    }
  }
  
  func testAddFavouriteMoviesWithExistMovie() {
    // Given
    let workerSpy = MovieDetailWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    interactor.worker = workerSpy
    let outputSpy = MovieDetailPresenterSpy()
    interactor.presenter = outputSpy
    let favouriteMovie = MovieModel(voteAverage: 7.1,
                                    overView: "Batman, Batgirl and Robin forge an alliance with the Teenage Mutant Ninja Turtles to fight against the Turtles' sworn enemy, The Shredder, who has apparently teamed up with Ra's Al Ghul and The League of Assassins.",
                                    releaseDate: "2019-03-31",
                                    title: "Batman vs. Teenage Mutant Ninja Turtles",
                                    posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let request = MovieDetail.FavouriteMovie.Request(favouriteMovie: favouriteMovie)
    
    // When
    interactor.addFavouriteMovies(request: request)
    
    // Then
    XCTAssert(workerSpy.getFavoriteMoviesCalled, "showMovieDetail should call getFavoriteMovies")
    XCTAssert(workerSpy.saveFavouriteMoviesCalled, "showMovieDetail should call saveFavouriteMovies")
    if let response = outputSpy.presentFavouriteStatusResponse?.favouriteStatus {
      XCTAssertEqual(response, false)
      XCTAssert(outputSpy.presentFavouriteStatusCalled, "showMovieDetail should call favouriteStatus")
    }
  }
  
  func testAddFavouriteMoviesWithNewMovie() {
    // Given
    let workerSpy = MovieDetailWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    interactor.worker = workerSpy
    let outputSpy = MovieDetailPresenterSpy()
    interactor.presenter = outputSpy
    let favouriteMovie = MovieModel(voteAverage: 7.1,
                                    overView: "Batman faces his ultimate challenge as the mysterious Red Hood takes Gotham City by firestorm. One part vigilante, one part criminal kingpin, Red Hood begins cleaning up Gotham with the efficiency of Batman, but without following the same ethical code.",
                                    releaseDate: "2010-07-27",
                                    title: "Batman: Under the Red Hood",
                                    posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let request = MovieDetail.FavouriteMovie.Request(favouriteMovie: favouriteMovie)
    
    // When
    interactor.addFavouriteMovies(request: request)
    
    // Then
    XCTAssert(workerSpy.getFavoriteMoviesCalled, "showMovieDetail should call getFavoriteMovies")
    XCTAssert(workerSpy.saveFavouriteMoviesCalled, "showMovieDetail should call saveFavouriteMovies")
    if let response = outputSpy.presentFavouriteStatusResponse?.favouriteStatus {
      XCTAssertEqual(response, true)
      XCTAssert(outputSpy.presentFavouriteStatusCalled, "showMovieDetail should call favouriteStatus")
    }
  }
  
  func testShowButtonText() {
    // Given
    let workerSpy = MovieDetailWorkerSpy(store: MoviesMockStore(), storage: DatabaseMockStore())
    interactor.worker = workerSpy
    let outputSpy = MovieDetailPresenterSpy()
    interactor.presenter = outputSpy
    let favouriteMovie = MovieModel(voteAverage: 7.1,
                                    overView: "Batman, Batgirl and Robin forge an alliance with the Teenage Mutant Ninja Turtles to fight against the Turtles' sworn enemy, The Shredder, who has apparently teamed up with Ra's Al Ghul and The League of Assassins.",
                                    releaseDate: "2019-03-31",
                                    title: "Batman vs. Teenage Mutant Ninja Turtles",
                                    posterURL: "https://image.tmdb.org/t/p/w92/7souLi5zqQCnpZVghaXv0Wowi0y.jpg")
    let request = MovieDetail.ShowButtonText.Request(favouriteMovie: favouriteMovie)
    
    // When
    interactor.showButtonText(request: request)
    
    // Then
    XCTAssert(workerSpy.getFavoriteMoviesCalled, "showMovieDetail should call getFavoriteMovies")
    XCTAssert(outputSpy.presentButtonTextCalled, "showMovieDetail should call presentButtonText")
  }
}
